# OmniBar for 3.3.5a client (WotLK, TBC, Vanilla)
**`OmniBar is an extremely lightweight addon that tracks enemy cooldowns.`**

<img src="http://i.imgur.com/p9DjSOh.png" width="50%">

---

### ⚠ Note: This version is not related or affiliated with the official retail/classic addon!
### 📥 [Installation](#-installation-1)
### 📋 [Report Issue](https://gitlab.com/Tsoukie/omnibar-3.3.5/-/issues)
### 💬 [FAQ](#-faq-1)
### ❤️ [Support & Credit](#%EF%B8%8F-support-credit-1)

---
### Features:
- **Customizable Cooldowns:** _Select which cooldowns you wish to track._
- **Multiple Bars:** _Create as many bars are you want!_
- **Automatically Hide Icons:** _After the cooldown is complete it will automatically hide the icon(s)._
- **Show Unused Icons:** _If you prefer to have icons remain visible._
- **Track Multiple Players:** _If another player is detected using the same ability, a duplicate icon will be created and tracked separately._
- **Glow Icons:** _A glow animation will be displayed around an icon when it is activated._
- **Visual Tweaks:** _You can configure various visual tweaks such as size, border, glow, transparency, columns, and padding._
- **Visibility:** _Choose to display OmniBar in arenas, battlegrounds, and world combat._
- **Profiles:** _Create, export and import custom profiles._

### Commands:
- **`/ob`** _Display config window_

<!-- blank line -->
<br>
<!-- blank line -->

# 📥 Installation

1. Download Latest Release `[.zip, .gz, ...]`:
	- <a href="https://gitlab.com/Tsoukie/omnibar-3.3.5/-/releases/permalink/latest" target="_blank">`📥 OmniBar-3.3.5`</a>
	- <a href="https://gitlab.com/Tsoukie/classicapi/-/releases/permalink/latest" target="_blank">`📥 ClassicAPI`</a> **⚠Required**
2. Extract **both** the downloaded compressed files _(eg. Right-Click -> Extract-All)_.
3. Navigate within each extracted folder(s) looking for the following: `!!!ClassicAPI` or `OmniBar`.
4. Move folder(s) named `!!!ClassicAPI` or `OmniBar` to your `Interface\AddOns\` folder.
5. Re-launch game.

<!-- blank line -->
<br>
<!-- blank line -->


# 💬 FAQ

> I found a bug!

Please 📋 [report the issue](https://gitlab.com/Tsoukie/omnibar-3.3.5/-/issues) with as much detail as possible.

<!-- blank line -->
<br>
<!-- blank line -->


# ❤️ Support & Credit
 
If you wish to show some support you can do so [here](https://streamlabs.com/tsoukielol/tip). Tips are completely voluntary and aren't required to download my projects, however, they are _very_ much appreciated. They allow me to devote more time to creating things I truly enjoy. 💜

<!-- blank line -->
<br>
<!-- blank line -->
  
_This_ version is modified and maintained by [Tsoukie](https://gitlab.com/Tsoukie/) and is **not** related or affiliated with any other version.

### Original AddOn
**Author:** [Jordon](https://github.com/jordonwow/) (not related/affiliated with this version)